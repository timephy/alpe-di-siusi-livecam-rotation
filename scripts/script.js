
const showTime = 60000;

const sites = [
    ["https://panodata.panomax.com/cams/1389/recent_hd.jpg", showTime],
    ["https://panodata.panomax.com/cams/1044/recent_hd.jpg", showTime],
    ["https://panodata.panomax.com/cams/1186/recent_hd.jpg", showTime],
    ["https://panodata.panomax.com/cams/1219/recent_hd.jpg", showTime],
    ["https://panodata.panomax.com/cams/1226/recent_hd.jpg", showTime],
    ["https://panodata.panomax.com/cams/1382/recent_hd.jpg", showTime],
    ["https://panodata.panomax.com/cams/1723/recent_hd.jpg", showTime / 5], //
    ["https://seiseralmgoldknopf.it-wms.com/big_panorama1.jpg", showTime],
    ["https://sassolevante.it-wms.com/big_panorama1.jpg", showTime],
    ["https://wolkenstein.it-wms.com/big_panorama1.jpg", showTime]
];

const weatherSites = [
    "https://www.bergfex.it/sommer/seiser-alm/wetter/berg/",
    "https://www.bergfex.it/groeden-seiser-alm/wetter/prognose/"
];

function time() {
    return new Date().getTime();
}

function decache(link, time) {
    return link + "?" + time;
}

const _new = (str) => document.createElement(str);

const $container = $("#container")[0];
console.log($container)

// SETUP WEATHER DIV

const weather = document.getElementById("weather");
const weatherLeftIframe = document.getElementById("weatherLeftIframe");
const weatherRightIframe = document.getElementById("weatherRightIframe");
const elements = [weather];

for (const link of sites) {
    const img = _new("img");
    img.style.display = "none";
    img.style["-webkit-transition"] = `${link[1] / 1000}s ease-in-out, opacity 2s ease-in-out`;
    img.style.opacity = 0;
    const currTime = time();
    link.push(currTime);
    img.src = decache(link[0], currTime);
    elements.push(img);
    $container.appendChild(img);
    // console.log(img.width);
    // break;
}
elements[0].style.opacity = 1;

let currPage = 0;

function incCurrPage() {
    currPage++;
    if (currPage >= elements.length) currPage = 0;
}

function offset(value, by) {
    const length = elements.length;
    const res = (value + by) % length;
    return res < 0 ? offset(res + length, length) : res;
}

function nextPage() {
    incCurrPage();
    //threat new one
    // console.log("showing:", currPage);
    elements[currPage].style.display = "";
    const val = -elements[currPage].clientWidth + $(window).width();
    elements[currPage].style.transform = `translateX(${val}px)`;
    elements[currPage].style.opacity = 1;
    const delay = currPage === 0 ? 20000 : sites[offset(currPage, -1)][1] - 2000;
    // console.log(delay);
    setTimeout(() => {
        nextPage();
    }, delay);
    //clear old one
    const old = offset(currPage, -1);
    // console.log("clearing:", old);
    //fix weather transition
    if (old === elements.length - 1) {
        elements[old].style.opacity = 0;
        // console.log("CALLED");
    }
    setTimeout(() => {
        elements[old].style.display = "none";
        elements[old].style.opacity = 0;
        elements[old].style.transform = `translateX(0px)`;
        if (old === 0) {
            //update weather
            weatherLeftIframe.src = decache(weatherSites[0], time());
            weatherRightIframe.src = decache(weatherSites[1], time());
        } else if (time() - sites[offset(old, -1)][2] > 40 * 60 * 1000) {
            // console.log("update", old);
            elements[old].src = sites[old - 1][0];
            //set new last time
            sites[offset(old, -1)][2] = time();
        }
    }, 2000);
}


setTimeout(() => {
    nextPage();
}, 10000);
